import React from "react";

export default function Specification({
  type,
  standard,
  thread,
  length,
  external,
  internal,
  remove,
}) {
  return (
    <div className="specification">
      <p>Спецификации</p>
      <div className="textSpecification">
        <p>Тип: </p>
        {type}
        <p>Стандарт: </p>
        {standard}
        <p>Резьба: </p>
        {thread}
        <p>Длина: </p>
        {length}
        <p>Внешний радиус: </p>
        {external}
        <p>Внутренний радиус : </p>
        {internal}
        <p>Шаг резьбы: </p>
        {remove}
      </div>
    </div>
  );
}
