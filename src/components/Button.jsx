import React from "react";

export default function Button({ className, message, onClick, disabled }) {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`button ${className}`}
    >
      {message}
    </button>
  );
}
