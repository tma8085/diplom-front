import React from "react";
import Button from "./Button";
import imageFront from "../img/front.png";
import imageSide from "../img/side.png";
import Specification from "./Specification";

export default function Modal({ onClick, type, standard, thread }) {
  return (
    <div className="modalWrapper">
      <div className="modal">
        <div className="contantWrap">
          <div className="imgModal">
            <div className="imgDownload">
              <div className="overlay" />
              <img alt="Гайка, вид спереди" src={imageFront} />
            </div>
            <div className="imgDownload">
              <div className="overlay" />
              <img alt="Гайка с боку" src={imageSide} />
            </div>
            <div className="imgDownload">
              <div className="overlay" />
              <img alt="Гайка, вид спереди" src={imageFront} />
            </div>
            <div className="imgDownload">
              <div className="overlay" />
              <img alt="Гайка с боку" src={imageSide} />
            </div>
          </div>
          <div className="results">
            <Specification />
          </div>
        </div>
        <p>Похожие гайки</p>
        <table className="table">
          <tr>
            <th>Тип</th>
            <th>Стандарт</th>
            <th>Резьба</th>
          </tr>
          <tr>
            <td>{type}</td>
            <td>{standard}</td>
            <td>{thread}</td>
          </tr>
        </table>
        <Button message="Закрыть" onClick={onClick} className="buttonBack" />
      </div>
    </div>
  );
}
