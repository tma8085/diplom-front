import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../img/logo.png";

function Header() {
  const [menuOpen, setMenuOpen] = React.useState(false);
  return (
    <header>
      <nav>
        <NavLink exact className="title" to="/">
          <img className="logo" alt="Гайка логотип" src={logo} />
          <span>Сервис идентификации гаек</span>
        </NavLink>
        <div
          className="nav-toggle"
          onClick={() => {
            setMenuOpen((o) => !o);
          }}
        >
          <a href="#" className={`menu example${menuOpen ? " active" : ""}`}>
            <span></span>
          </a>
        </div>
        <ul className={`header__nav ${menuOpen ? "" : "hidden"}`}>
          <li>
            <NavLink
              exact
              className="link"
              activeClassName="active"
              to="/database"
            >
              База гаек
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              className="link"
              activeClassName="active"
              to="/about"
            >
              О программе
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
