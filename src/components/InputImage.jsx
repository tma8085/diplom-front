import React from 'react'

export default function InputImage({src}) {
    return (
        <div className="input-image">
            <img src={src} alt=""/>
            
        </div>
    )
}
