import React, { Component } from "react";
import imageFront from "../img/front.png";
import imageSide from "../img/side.png";
import Button from "../components/Button";
import Modal from "../components/Modal";
import Specification from "../components/Specification";

import {imageToArray, arrayToImage, getImageObject, binarization, cras, multyCras, zone, multyClear} from '../utils/cv'

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      file: undefined,
      imagePreviewUrl: "",
      file2: undefined,
      imagePreviewUrl2: "",
      zoomFactor: 1,
    };
    
    this.canvasRef = React.createRef();
  }

  openHandler = (state) => {
    this.setState({
      isOpen: state,
    });
  };

  handleImageChange = (e) => {
      e.preventDefault();
      const reader = new FileReader();
      const file = e.target.files[0];
      const canvas = this.canvasRef.current
      const ctx = canvas.getContext('2d');
      reader.onloadend = (event) => {
        var img = new Image();
        img.onload = ()=>{
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
            const imgObject = getImageObject(ctx,0,0,img.width,img.height)
            this.setState({
              imgObject:imgObject
            },()=>{this.detectOnImage()})
        }
        img.src = event.target.result;
        this.setState({
          file,
          imagePreviewUrl: reader.result,
        });
      };
      reader.readAsDataURL(file);
  };

  detectOnImage = () => {
    const canvas = this.canvasRef.current
    const ctx = canvas.getContext('2d')
    let { imgObject } = this.state

    imgObject = binarization(imgObject)
    imgObject = multyCras(imgObject)
    imgObject = multyClear(imgObject)
    // imgObject = zone(imgObject)
    console.log('imgObjectCras', imgObject)
    arrayToImage(imgObject,ctx)
  }

  handleImageChange2 = (e) => {
    try {
      e.preventDefault();
      const reader = new FileReader();
      const file2 = e.target.files[0];
      reader.onloadend = () => {
        console.log("reader.result", reader.result);
        this.setState({
          file2,
          imagePreviewUrl2: reader.result,
        });
      };
      reader.readAsDataURL(file2);
    } catch {
      return;
    }
  };
  
  componentDidMount() {
    // this.updateCanvas();
  }

  updateCanvas=()=>{
      const ctx = this.canvasRef.current.getContext('2d');
      // ctx.fillRect(0,0, 100, 100);
  }
  render() {
    const { isOpen, imagePreviewUrl, imagePreviewUrl2, zoomFactor } = this.state;
    return (
      <main className="card">
        <h1>Инструкция по использованию сервиса</h1>
        <div className="mainContent">
          <div className="textMain">
            <p>
              1. Загрузите две фотографии гайки. В двух разных проекциях, как
              указано на картинках снизу.
            </p>
            <p>2. Нажмите кнопку “Идентификация”.</p>
            <p>3. Дождитесь обработки изображения.</p>
          </div>
          <div className="imageWrapper">
            <div className="bMain">
              <div className="imgDownload">
                <div className="overlay" />
                <img
                  alt="Гайка, вид спереди"
                  src={imagePreviewUrl || imageFront}
                />
              </div>
              <div className="inputWraper">
                <input type="file" onChange={this.handleImageChange} />
                <Button message="ЗАГРУЗИТЬ" className="bUpload" />
              </div>
            </div>
            {/* <div className="bMain">
              <div className="imgDownload">
                <div className="overlay" />
                <img alt="Гайка с боку" src={imagePreviewUrl2 || imageSide} />
              </div>
              <div className="inputWraper">
                <input type="file" onChange={this.handleImageChange2} />
                <Button message="ЗАГРУЗИТЬ" className="bUpload" />
              </div>
            </div> */}
          </div>
          {/* <Button onClick={this.drowNext} message="следующий шаг"/> */}
          <input type="number" value={zoomFactor} onChange={(e)=>{this.setState({zoomFactor:e.target.value})}} />
          <div className="canvasWrapper">
            <canvas style={{width:`${zoomFactor*100}%`,height:`${zoomFactor*100}%`}} className="canvas" ref={this.canvasRef} />
          </div>
        </div>
        <aside>
          <div className="result">
            <Specification />
            <p className="sError">Сообщить об ошибке</p>
            <Button
              message="Подробно"
              onClick={() => {
                this.openHandler(true);
              }}
              className="more"
            />
          </div>
          <Button
            message="Идентификация"
            disabled={!imagePreviewUrl || !imagePreviewUrl2}
            className="bresult"
          />
        </aside>
        {isOpen && (
          <Modal
            onClick={() => {
              this.openHandler(false);
            }}
          />
        )}
      </main>
    );
  }
}
