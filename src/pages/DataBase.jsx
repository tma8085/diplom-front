import React from "react";
import lense from "../img/lense.svg";
import Modal from "../components/Modal";

export default class DataBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  openHandler = (state) => {
    this.setState({
      isOpen: state,
    });
  };

  render() {
    const { isOpen } = this.state;
    return (
      <main className="cardBase">
        <div className="topBar">
          <div className="lense">
            <input type="text" placeholder="Поиск" />
            <img src={lense} alt="Лупа" />
          </div>
        </div>
        <table>
          <tr>
            <th>Тип</th>
            <th>Стандарт</th>
            <th>Резьба</th>
          </tr>
          <tr
            onClick={() => {
              this.openHandler(true);
            }}
          >
            <td>Удлиненная гайка</td>
            <td>DIN934</td>
            <td>Метрическая</td>
          </tr>
        </table>
        {isOpen && (
          <Modal
            onClick={() => {
              this.openHandler(false);
            }}
          />
        )}
      </main>
    );
  }
}
