export const imageToArray = (
  context,
  x,
  y,
  width,
  height,
  grayscale = true
) => {
  let imgd = context.getImageData(x, y, width, height);
  let pix = imgd.data;

  let matrix = [];
  for (let row = 0; row < height; row += 1) {
    let matrixRow = [];
    for (let col = 0; col < width; col += 1) {
      if (grayscale) {
        matrixRow.push(
          (pix[(row * width + col) * 4] +
            pix[(row * width + col) * 4 + 1] +
            pix[(row * width + col) * 4 + 2]) /
            3
        );
      } else {
        matrixRow.push({
          r: pix[(row * width + col) * 4],
          g: pix[(row * width + col) * 4 + 1],
          b: pix[(row * width + col) * 4 + 2],
        });
      }
    }
    matrix.push(matrixRow);
  }
  return matrix;
};

export const arrayToImage = (imgObject, context) => {
  const { p, width, height } = imgObject;

  let imgd = context.getImageData(0, 0, width, height);
  let pix = imgd.data;

  for (let row = 0; row < height; row += 1) {
    for (let col = 0; col < width; col += 1) {
      pix[(row * width + col) * 4] = p[row][col].r || p[row][col];
      pix[(row * width + col) * 4 + 1] = p[row][col].g || p[row][col];
      pix[(row * width + col) * 4 + 2] = p[row][col].b || p[row][col];
    }
  }
  context.imageSmoothingEnabled = false;
  context.putImageData(imgd, 0, 0);

  let center = findCenter(imgObject);

  const fp = firstPixel(imgObject);
  context.fillStyle = "#FF0000";
  context.fillRect(fp.x, fp.y, 1, 1);
  coolZone(fp.x, fp.y, imgObject, context);

  const centerTop = topFirstPixel(center.x, center.y, imgObject);
  context.fillStyle = "#FF0000";
  context.fillRect(centerTop.x, centerTop.y, 1, 1);
  coolZone(centerTop.x, centerTop.y, imgObject, context, 4);

  const fillPoint = startFillPoint(fp.x, fp.y, imgObject);
  fillInside(
    fillPoint.x + 3,
    fillPoint.y + 3,
    imgObject,
    context,
    imgObject.height
  );

  //drow center
  context.beginPath();
  context.arc(center.x, center.y, 5, 0, 2 * Math.PI);
  context.strokeStyle = "#FF000088";
  context.stroke();
  context.fillStyle = "#0000FF88";
  context.fill();
};

export const startFillPoint = (_x, _y, _image) => {
  console.log("image", _image);
  let startFill = { x: -1, y: -1 };
  for (let i = _y, j = _x; i < _image.height || j < _image.height; i++, j++) {
    if (_image.p[i][j] === 0) {
      startFill.x = j;
      startFill.y = i;
      return startFill;
    }
  }
  return startFill;
};

export default imageToArray;

export const getImageObject = (
  context,
  x,
  y,
  width,
  height,
  grayscale = true
) => {
  let arr = imageToArray(context, x, y, width, height);
  return { p: arr, width, height };
};

export const binarization = (img, factor = 1.6) => {
  let image = img;

  let pixelSum = 0;
  let S = 0;
  let D = 0;

  console.log("length", img.width * img.height);

  for (let i = 0; i < image.height; i++) {
    for (let j = 0; j < image.width; j++) {
      pixelSum += image.p[i][j];
    }
  }

  console.log("pixelSum", pixelSum);

  let averange = pixelSum / (image.height * image.width);

  console.log("averange", averange);

  for (let i = 0; i < image.height; i++) {
    for (let j = 0; j < image.width; j++) {
      S += Math.pow(image.p[i][j] - averange, 2);
    }
  }

  D = S / (image.height * image.width);
  let SO = Math.sqrt(D);
  let SOFACTOR = SO * factor;

  console.log("D", D);
  console.log("SO", SO);
  console.log("SOFACTOR", SOFACTOR);

  for (let i = 0; i < image.height; i++) {
    for (let j = 0; j < image.width; j++) {
      // console.log('(Math.abs(image.p[i][j])-averange)', (Math.abs(image.p[i][j])-averange))
      if (Math.abs(image.p[i][j] - averange) > SOFACTOR) {
        image.p[i][j] = 0;
      } else {
        image.p[i][j] = 255;
      }
    }
  }

  return image;
};

export const cras = (img) => {
  const _imageObject = img;
  const { height, width, p: image } = _imageObject;
  let flag = 0;
  for (let i = 0; i < height - 1; i++) {
    for (let j = 0; j < width - 1; j++) {
      if (i !== 0) {
        if (j !== 0) {
          let sumPixels =
            image[i - 1][j - 1] +
            image[i][j - 1] +
            image[i + 1][j - 1] +
            image[i - 1][j] +
            image[i + 1][j] +
            image[i - 1][j + 1] +
            image[i][j + 1] +
            image[i + 1][j + 1];
          if (sumPixels < 254 * 4) {
            if (image[i][j] !== 0) {
              image[i][j] = 0;
              flag += 1;
            }
          }
        }
      }
    }
  }
  _imageObject.p = image;
  return { image: _imageObject, flag };
};

export const multyCras = (image, times = 15) => {
  for (let i = 0; i < times; i++) {
    let { image: _image, flag } = cras(image);
    if (flag == 0) {
      return _image;
    }
  }
  return image;
};

export const clear = (img) => {
  const _imageObject = img;
  const { height, width, p: image } = _imageObject;

  let flag = 0;
  for (let i = 0; i < height - 1; i++) {
    for (let j = 0; j < width - 1; j++) {
      if (i != 0) {
        if (j != 0) {
          let sumPixels =
            image[i - 1][j - 1] +
            image[i][j - 1] +
            image[i + 1][j - 1] +
            image[i - 1][j] +
            image[i + 1][j] +
            image[i - 1][j + 1] +
            image[i][j + 1] +
            image[i + 1][j + 1];
          if (sumPixels > 254 * 6) {
            if (image[i][j] != 255) {
              image[i][j] = 255;
              flag += 1;
            }
          }
        }
      }
    }
  }

  _imageObject.p = image;
  return { image: _imageObject, flag };
};

export const multyClear = (image, times = 15) => {
  for (let i = 0; i < times; i++) {
    let { image: _image, flag } = clear(image);
    if (flag == 0) {
      return _image;
    }
  }
  return image;
};

export const zone = (img) => {
  const _imageObject = img;
  const { height, width, p: zoneImage } = _imageObject;

  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      if (zoneImage[row][col] == 0) {
        zoneImage[row][col] = 127;
        break;
      }
    }
  }
  for (let col = 0; col < width; col++) {
    for (let row = 0; row < height; row++) {
      if (zoneImage[row][col] == 0) {
        zoneImage[row][col] = 127;
        break;
      }
    }
  }
  for (let row = height - 1; row >= 0; row--) {
    for (let col = width - 1; col >= 0; col--) {
      if (zoneImage[row][col] == 0) {
        zoneImage[row][col] = 127;
        break;
      }
    }
  }
  for (let col = width - 1; col >= 0; col--) {
    for (let row = height - 1; row >= 0; row--) {
      if (zoneImage[row][col] == 0) {
        zoneImage[row][col] = 127;
        break;
      }
    }
  }

  return _imageObject;
};

export const findCenter = (img) => {
  const _imageObject = img;
  const { height, width, p } = _imageObject;

  let positionY = 0;
  let positionX = 0;
  let valFactor = 0;

  for (let i = 0; i < height - 1; i++) {
    for (let j = 0; j < width - 1; j++) {
      let k = p[i][j];
      if (k < 255) {
        valFactor++;
        positionX = positionX + j;
        positionY = positionY + i;
      }
    }
  }

  positionX = positionX / valFactor;
  positionY = positionY / valFactor;

  return { x: positionX, y: positionY };
  // print('row=',positionX,'cols=',positionY)
  // print(valFactor)
  // cv.circle(img, (int(positionX),int(positionY)), 10, (255,10,10),10)
  // cv.circle(thresh, (int(positionX),int(positionY)), 10, 50,10)
};

export const firstPixel = (img) => {
  const _imageObject = img;
  const { height, width, p: zoneImage } = _imageObject;

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      if (zoneImage[i][j] == 0) {
        return { x: j, y: i };
      }
    }
  }
};

export const topFirstPixel = (centerX, centerY, img) => {
  const cx = Math.ceil(centerX);
  const cy = Math.ceil(centerY);
  const _imageObject = img;
  const { height, width, p: zoneImage } = _imageObject;

  console.log("zoneImage", _imageObject.p[cy][cx]);
  for (let i = cy; i > 0; i--) {
    console.log("zoneImage[centerX][i]", zoneImage[i][cx]);
    if (zoneImage[i][cx] == 0) {
      return { x: cx, y: i };
    }
  }
  return { x: -1, y: -1 };
};

export const nextDirection = (direction) => {
  let _direction = direction + 1;
  return _direction % 8;
};

const dirs = [
  { x: 0, y: -1 },
  { x: 1, y: -1 },
  { x: 1, y: 0 },
  { x: 1, y: 1 },
  { x: 0, y: 1 },
  { x: -1, y: 1 },
  { x: -1, y: 0 },
  { x: -1, y: -1 },
];

export const findBlack = (firstX, firstY, img, context, direction = 6) => {
  const _imageObject = img;
  const { height, width, p: image } = _imageObject;

  let currDir = direction;

  for (let i = 0; i < 8; i++) {
    currDir = nextDirection(currDir);
    let { x: modX, y: modY } = dirs[currDir];
    if (image[firstY + modY][firstX + modX] == 0) {
      image[firstY + modY][firstX + modX] = 127;
      return { x: firstX + modX, y: firstY + modY, direction: currDir + 4 };
    }
  }
};

const coolZone = (firstX, firstY, image, context, _direction = 6) => {
  const pathsArray = [{ x: firstX, y: firstY }];
  let x = firstX;
  let y = firstY;
  let direction = _direction;
  do {
    const data = findBlack(x, y, image, context, direction);
    x = data.x;
    y = data.y;
    direction = data.direction;
    context.fillStyle = "#00FF00";
    context.fillRect(x, y, 1, 1);
    pathsArray.push({ x, y });
  } while (pathsArray[0].x !== x || pathsArray[0].y !== y);
  return pathsArray;
};

export const fillInside = (_x, _y, _image, context, deep = 200) => {
  const { height, width, p: image } = _image;
  if (deep <= 0) {
    return;
  }
  context.fillStyle = "#FF00FF33";
  context.fillRect(_x, _y, 1, 1);

  let firstX = _x;
  let lastX = _image.height - 1;

  for (let x = _x; x < _image.width; x++) {
    if (image[_y][x] == 127 || image[_y][x] == 64) {
      break;
    } else {
      image[_y][x] = 64;
      context.fillStyle = "#FF00FF33";
      context.fillRect(x, _y, 1, 1);
      lastX = x;
    }
  }

  for (let x = _x - 1; x > 0; x--) {
    if (image[_y][x] == 127 || image[_y][x] == 64) {
      break;
    } else {
      image[_y][x] = 64;
      firstX = x;
      context.fillStyle = "#FF00FF33";
      context.fillRect(x, _y, 1, 1);
    }
  }

  for (let x = firstX; x < lastX; x++) {
    if (image[_y - 1][x] !== 64) {
      if (image[_y - 1][x] !== 127) {
        console.info(`${_y - 1} ${x} =`, image[_y - 1][x]);
        fillInside(x, _y - 1, _image, context, deep - 1);
      }
    }
  }

  for (let x = firstX; x < lastX; x++) {
    if (image[_y + 1][x] !== 64) {
      if (image[_y + 1][x] !== 127) {
        console.info(`${_y + 1} ${x} =`, image[_y + 1][x]);
        fillInside(x, _y + 1, _image, context, deep - 1);
      }
    }
  }
};

export const Counter = () => {
  let km = 0;
  let kn = 0;
  let cur = 1;
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      kn = j-1;
      if (kn <= 0){
        kn = 1;
        B = 0; 
      }
      else{
        B = image[i,kn];
      }
      km = i - 1;
    if (km <= 0){
      km = 1;
      C = 0;}
    else{
      C = image[km,j];
    }
    A = image[i,j];
    if (A == 0){}
    else{if ((B == 0) & (C == 0)){
        cur = cur + 1;
        image[i,j] = cur;}
    else{if ((B != 0) & (C == 0)){
        image[i,j] = B;}
    else{if ((B == 0) & (C != 0)){
        image[i,j] = C;}
    else{if ((B != 0) & (C != 0)){      
        if (B == C){
          image[i,j] = B;}
        else{
          image[i,j] = B;
          image(image == C) = B;}}}}}
    }
  }
};
