import React from "react";
import Header from "./components/Header";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Main from "./pages/Main";
import About from "./pages/About";
import DataBase from './pages/DataBase';

class App extends React.Component{
  render(){
    return (
      <div className="app">
        <Router>
          <Header />
          <Route exact path="/">
            <Main />
          </Route>
          <Route exact path="/database">
            <DataBase />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
        </Router>
      </div>
    );
  }
}

export default App;